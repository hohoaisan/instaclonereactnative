import React from 'react';

import {View, TouchableOpacity, Image, StyleSheet} from 'react-native';

const CardItem = ({item}) => {
  return (
    <View style={styles.cardContainer}>
      <TouchableOpacity>
        <Image
          source={{uri: item.uri, width: 200, height: 200}}
          style={styles.cardImage}
          resizeMode="center"
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    width: '33%',
    height: 50,
    borderColor: 'black',
    aspectRatio: 1,
    overflow: 'hidden',
    justifyContent: 'center',
    alignContent: 'center',
  },
  cardImage: {
    width: 100,
    height: 100,
    alignSelf: 'center',
  },
});

export default CardItem;

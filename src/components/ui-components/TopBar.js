import React from 'react';

import {View, Text, StyleSheet} from 'react-native';

const TopBar = ({children, style}) => {
  return (
    <View style={styles.TopBar}>
      <View style={style}>
        <View style={styles.controls}>
          <View
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
            }}>
            {children[0]}
          </View>
          <View style={{padding: 10}}>
            <Text style={{color: 'white', fontSize: 20, fontWeight: 'bold'}}>
              {children[1]}
            </Text>
          </View>
          <View
            style={{
              position: 'absolute',
              top: 0,
              right: 0,
            }}>
            {children[2]}
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  TopBar: {
    top: 0,
    zIndex: 1,
    left: 0,
    width: '100%',
    padding: 10,
  },
  controls: {
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
  },
});
export default TopBar;

import * as React from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TextInput,
  Pressable,
} from 'react-native';

const CustomTextInput = ({label, value, type, icon, style, ...inputProps}) => {
  let secureTextEntry = null;
  switch (type) {
    case 'password':
      secureTextEntry = true;
      break;
    default:
      secureTextEntry = false;
  }
  return (
    <Pressable>
      <View
        style={{
          flexDirection: 'row',
          borderRadius: 5,
          alignItems: 'center',
          paddingHorizontal: 6,
          paddingVertical: 2,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,
          elevation: 2,
          backgroundColor: 'white',
          ...style,
        }}>
        {/* <View style={{padding: 10}}>
        <Image source={icon} style={{width: 20, height: 20}} />
      </View> */}
        <View
          style={{
            flexShrink: 1,
            width: '100%',
          }}>
          <View>
            <Text
              style={{
                textTransform: 'uppercase',
                color: 'gray',
                padding: 3,
                paddingBottom: 0,
                fontSize: 10,
              }}>
              {label}
            </Text>
            <TextInput
              numberOfLines={1}
              style={{
                fontSize: 18,
                padding: 3,
                paddingTop: 0,
                fontSize: 15,
              }}
              {...inputProps}
              secureTextEntry={secureTextEntry}
            />
          </View>
        </View>
      </View>
    </Pressable>
  );
};
CustomTextInput.defaultProps = {
  label: 'sample label',
  value: '',
  type: 'default',
  icon: undefined,
};
export default CustomTextInput;

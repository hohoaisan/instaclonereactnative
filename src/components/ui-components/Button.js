import React from 'react';
import {TouchableNativeFeedback, View, Text, StyleSheet} from 'react-native';

const CustomButton = ({children, onPress, color}) => {
  return (
    <View
      style={{
        backgroundColor: color,
        borderRadius: 10,
        elevation: 2,
        overflow: 'hidden',
      }}>
      <TouchableNativeFeedback onPress={onPress ? onPress : undefined}>
        <View
          style={{
            padding: 15,
          }}>
          <Text
            style={{
              color: 'white',
              fontSize: 15,
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            {children}
          </Text>
        </View>
      </TouchableNativeFeedback>
    </View>
  );
};

CustomButton.defaultProps = {
  color: '#71a3ff',
};

export default CustomButton;

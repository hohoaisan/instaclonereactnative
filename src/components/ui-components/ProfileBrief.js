import React from 'react';
import {View, Image, Text} from 'react-native';

const ProfileBrief = ({image, title, subtitle}) => {
  return (
    <View style={{flexDirection: 'row'}}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: 85,
            height: 85,
            backgroundColor: 'white',
            borderRadius: 200,
            overflow: 'hidden',
          }}>
          <Image
            source={image}
            style={{width: 80, height: 80, borderRadius: 200}}
          />
        </View>
      </View>
      <View style={{flexShrink: 1, marginLeft: 10}}>
        <View>
          <Text
            numberOfLines={1}
            style={{
              color: '#FFFFFF',
              fontSize: 20,
            }}>
            {title}
          </Text>
          <Text
            numberOfLines={1}
            style={{
              color: '#FFFFFF',
              fontSize: 15,
            }}>
            {subtitle}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default ProfileBrief;

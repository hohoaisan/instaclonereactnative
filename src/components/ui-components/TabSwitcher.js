import React from 'react';
import {View, Text, TouchableNativeFeedback, StyleSheet} from 'react-native';

const TabSwitcherButton = ({active, children}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: active ? 'white' : undefined,
        borderRadius: 50,
        overflow: 'hidden',
      }}>
      <TouchableNativeFeedback>
        <View
          style={{
            flex: 1,
            width: '100%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{color: active ? '#60b075' : 'gray', fontWeight: 'bold'}}>
            {children}
          </Text>
        </View>
      </TouchableNativeFeedback>
    </View>
  );
};

const TabSwitcher = (props) => {
  return (
    <View style={styles.TabSwitcher}>
      <TabSwitcherButton active>Posts</TabSwitcherButton>
      <TabSwitcherButton>Likes</TabSwitcherButton>
    </View>
  );
};
const styles = StyleSheet.create({
  TabSwitcher: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    backgroundColor: '#f6f6f6',
    borderRadius: 40,
    padding: 2,
    borderColor: '#dddddd',
    borderWidth: 1,
  },
});
export default TabSwitcher;

import React from 'react';
import {View, Pressable, Text} from 'react-native';
const PressableText = ({onPress, style, children, text, align}) => {
  return (
    <View style={{flexDirection: 'row', flex: 1, justifyContent: align}}>
      <Text style={{...style, color: '#000000'}}>
        {children ? `${children} ` : ''}
      </Text>
      <Pressable onPress={onPress ? onPress : undefined}>
        <Text style={style}>{text}</Text>
      </Pressable>
    </View>
  );
};

PressableText.defaultProps = {
  style: {
    color: '#71a3ff',
    fontWeight: 'bold',
  },
  align: 'center',
  text: '',
  children: '',
  onPress: () => {
    console.log('pressed');
  },
};
export default PressableText;

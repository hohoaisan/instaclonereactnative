import React, {useState, useEffect, useContext} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StyleSheet,
  Pressable,
  Image,
} from 'react-native';
import {TextInput, Button, PressableText} from '../components/ui-components';

const headingImg = require('../assets/images/ouch-ginger-cat-done.png');
const SingupScreen = ({navigation}) => {
  let [username, setUsername] = useState('');
  let [password, setPassword] = useState('');
  let [fullname, setFullName] = useState('');
  let [email, setEmail] = useState('');
  let [dateOfBirth, setDateOfBirth] = useState(new Date());

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.componentVerticalGap}>
            <View style={styles.componentVerticalGap}>
              <Text style={styles.headingText}>
                Capture your life, take your every soul
              </Text>
            </View>
            <View style={styles.componentVerticalGap}>
              <Text style={styles.subheadingText}>
                Create an account to explore more
              </Text>
            </View>
          </View>

          <View style={styles.componentVerticalGap}>
            <View>
              <Image
                source={headingImg}
                style={styles.headingImage}
                resizeMode="cover"
              />
            </View>
          </View>
          <View style={styles.componentVerticalGap}>
            <TextInput
              label="Username"
              value={username}
              onChangeText={(text) => {
                setUsername(text);
              }}
            />
          </View>
          <View style={styles.componentVerticalGap}>
            <TextInput
              label="Password"
              type="password"
              value={password}
              onChangeText={(text) => {
                setPassword(text);
              }}
            />
          </View>
          <View style={styles.componentVerticalGap}>
            <TextInput
              label="Full name"
              value={fullname}
              onChangeText={(text) => {
                setFullName(text);
              }}
            />
          </View>
          <View style={styles.componentVerticalGap}>
            <TextInput
              label="Email"
              value={email}
              onChangeText={(text) => {
                setEmail(text);
              }}
            />
          </View>
          <View style={styles.componentVerticalGap}>
            <TextInput label="Date of birth" value={dateOfBirth} />
          </View>
          <View style={styles.componentVerticalGap}>
            <Button
              color="#ff7d7d"
              onPress={() => navigation.navigate('LoginScreen')}>
              Sign up
            </Button>
          </View>

          <View style={styles.componentVerticalGap}>
            <PressableText
              text="Sign in"
              align="center"
              onPress={() => navigation.navigate('LoginScreen')}>
              Already have an account?
            </PressableText>
          </View>
          <View />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  container: {
    paddingHorizontal: 20,
    paddingBottom: 30,
  },
  componentVerticalGap: {
    marginVertical: 5,
  },
  headingText: {
    color: '#71a3ff',
    fontWeight: 'bold',
    width: '90%',
    fontSize: 30,
  },
  subheadingText: {
    color: 'gray',
  },
  headingImage: {
    width: 250,
    height: 150,
    alignSelf: 'center',
  },
});
export default SingupScreen;

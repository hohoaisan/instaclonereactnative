import React, {useState, useEffect, useContext} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StyleSheet,
  Pressable,
  Image,
} from 'react-native';
import {TextInput, Button, PressableText} from '../components/ui-components';

const headingImg = require('../assets/images/ouch-ginger-cat-camera-access.png');
const LoginScreen = ({navigation}) => {
  let [username, setUsername] = useState('');
  let [password, setPassword] = useState('');
  return (
    <SafeAreaView style={styles.root}>
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.componentVerticalGap}>
            <View style={styles.componentVerticalGap}>
              <Text style={styles.headingText}>
                Capture your life, take your every soul
              </Text>
            </View>
            <View style={styles.componentVerticalGap}>
              <Text style={styles.subheadingText}>
                Please login to continue using our app
              </Text>
            </View>
          </View>

          <View style={styles.componentVerticalGap}>
            <View>
              <Image
                source={headingImg}
                style={styles.headingImage}
                resizeMode="cover"
              />
            </View>
          </View>
          <View style={styles.componentVerticalGap}>
            <TextInput
              label="Username"
              value={username}
              onChangeText={(text) => {
                setUsername(text);
              }}
            />
          </View>
          <View style={styles.componentVerticalGap}>
            <TextInput
              label="Password"
              type="password"
              value={password}
              onChangeText={(text) => {
                setPassword(text);
              }}
            />
          </View>
          <View style={styles.componentVerticalGap}>
            <PressableText text="Forgot password?" align="flex-end" />
          </View>
          <View style={styles.componentVerticalGap}>
            <Button onPress={() => navigation.navigate('ProfileScreen')}>
              Sign in
            </Button>
          </View>

          <View style={styles.componentVerticalGap}>
            <PressableText
              text="Sign up"
              align="center"
              onPress={() => navigation.navigate('SignupScreen')}>
              Don't have an account?
            </PressableText>
          </View>
          <View />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  container: {
    paddingHorizontal: 20,
  },
  componentVerticalGap: {
    marginVertical: 5,
  },
  headingText: {
    color: '#71a3ff',
    fontWeight: 'bold',
    width: '90%',
    fontSize: 30,
  },
  subheadingText: {
    color: 'gray',
  },
  headingImage: {
    width: 200,
    height: 200,
    aspectRatio: 1,
    alignSelf: 'center',
  },
});
export default LoginScreen;

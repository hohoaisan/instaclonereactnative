import React, {useState, useEffect, useContext} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StyleSheet,
  Pressable,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
const profile = require('../assets/images/profile.jpg');
import {
  TextInput,
  Button,
  PressableText,
  TopBar,
  ProfileBrief,
  TabSwitcher,
  CardItem,
} from '../components/ui-components';

const photos = [
  {
    id: '1',
    uri: 'https://picsum.photos/200',
  },
  {id: '2', uri: 'https://picsum.photos/201'},
  {id: '3', uri: 'https://picsum.photos/202'},
  {
    id: '4',
    uri: 'https://picsum.photos/203',
  },
  {
    id: '5',
    uri: 'https://picsum.photos/204',
  },
  {
    id: '6',
    uri: 'https://picsum.photos/205',
  },
  {
    id: '7',
    uri: 'https://picsum.photos/200',
  },
  {
    id: '8',
    uri: 'https://picsum.photos/201',
  },
  {
    id: '9',
    uri: 'https://picsum.photos/202',
  },
  {
    id: '10',
    uri: 'https://picsum.photos/203',
  },
  {
    id: '11',
    uri: 'https://picsum.photos/200',
  },
  {
    id: '12',
    uri: 'https://picsum.photos/201',
  },
  {
    id: '13',
    uri: 'https://picsum.photos/202',
  },
  {
    id: '14',
    uri: 'https://picsum.photos/203',
  },
];
const ProfileScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.root}>
      <View>
        <View style={styles.headingSection}>
          <TopBar>
            <Button onPress={() => navigation.navigate('FriendsScreen')}>
              Friends
            </Button>
            <Text>Profile</Text>
            <Button onPress={() => navigation.navigate('LoginScreen')}>
              Logout
            </Button>
          </TopBar>
          <View style={[styles.container, {paddingVertical: 10}]}>
            <View style={styles.componentVerticalGap}>
              <ProfileBrief
                image={profile}
                title="hohoaisan"
                subtitle="a code-very-slow guy yes yes yes yes yes yes yes yes yes yes "
              />
            </View>
            <View style={styles.componentVerticalGap}>
              <TabSwitcher />
            </View>
          </View>
        </View>
      </View>
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.postContainer}>
            {photos.map((item, index) => (
              <CardItem item={item} key={index} />
            ))}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  container: {
    paddingHorizontal: 20,
  },
  componentVerticalGap: {
    marginVertical: 5,
  },
  headingSection: {
    backgroundColor: '#71a3ff',
    elevation: 3,
    zIndex: 0,
  },
  postContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignContent: 'center',
    marginVertical: 20,
  },
});
export default ProfileScreen;

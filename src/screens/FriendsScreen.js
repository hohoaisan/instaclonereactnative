/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  Image,
  ImageBackground,
  TouchableNativeFeedback,
} from 'react-native';
import {TopBar, Button} from '../components/ui-components';
const backgroundImage = require('../assets/background.png');
const Card = ({title = '', img, description = ''}) => {
  return (
    <View
      style={{
        borderRadius: 10,
        overflow: 'hidden',
        width: '100%',
        height: 80,
      }}>
      <TouchableNativeFeedback>
        <View
          style={{
            padding: 10,
            backgroundColor: '#71a3ff4f',
            width: '100%',
            height: '100%',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={{
                height: '100%',
                aspectRatio: 1,
                marginRight: 20,
                borderRadius: 100,

                overflow: 'hidden',
              }}>
              <Image
                source={{
                  uri: img,
                  width: 100,
                  height: 100,
                }}
                style={{width: '100%', height: '100%'}}
              />
            </View>
            <View style={{flexShrink: 1}}>
              <Text style={{color: 'black', fontSize: 18}}>{title}</Text>
              <View>
                <Text
                  numberOfLines={2}
                  style={{
                    color: 'black',
                  }}>
                  {description}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableNativeFeedback>
    </View>
  );
};

const GridCell = (WrappedComponent) => {
  return (props) => {
    return (
      <View
        style={{
          width: '100%',
          padding: 5,
        }}>
        <WrappedComponent {...props} />
      </View>
    );
  };
};

const CardWarped = GridCell(Card);
const FriendsScreen = ({navigation}) => {
  const listItem = [
    {
      title: 'Facebook',
      img: 'https://picsum.photos/100',
      description: 'Facebook is a social media platform',
    },
    {
      title: 'Messenger',
      img: 'https://picsum.photos/101',
      description:
        'Messenger is a social media platform mostly focus on texting between people',
    },
    {
      title: 'Twitter',
      img: 'https://picsum.photos/102',
      description: 'Twitter is a social media platform too',
    },
    {
      title: 'Code React Native',
      img: 'https://picsum.photos/103',
      description: 'Nothing in particullar',
    },
    {
      title: 'Not thing important',
      img: 'https://picsum.photos/104',
      description: 'Yes',
    },
  ];
  return (
    <View style={{flex: 1}}>
      <View style={styles.headingSection}>
        <TopBar>
          <></>
          <Text>Friends</Text>
          <Button onPress={() => navigation.navigate('ProfileScreen')}>
            Profile
          </Button>
        </TopBar>
      </View>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
        }}>
        <ScrollView style={{backgroundColor: 'transfarent'}}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              flexWrap: 'wrap',
            }}>
            {listItem.map((item, index) => (
              <CardWarped {...item} key={index} />
            ))}
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  headingSection: {
    backgroundColor: '#71a3ff',
    elevation: 3,
    zIndex: 0,
  },
});
export default FriendsScreen;
